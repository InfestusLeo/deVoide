import discord, asyncio
from discord.ext import commands
import CONFIG as config
import json

startup_extensions = ["moderation","suggestions","setup","vg_api","poll"]

bot = commands.Bot(command_prefix='??')

def is_me(m):
    return m.author == bot.user


## DONE v2.0.1
@bot.event
async def on_ready():
    """
    Performs certain tasks when the bot boots up.

    """
    
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    with open('settings.json','r') as f:
        sett = json.load(f)
        print("Total Servers = ",len(sett.keys()))
    print('------')
    game = discord.Game('Vainglory in {} servers'.format(len(sett.keys())))
    await bot.change_presence(status = discord.Status.idle, activity = game)
    

## DONE v2.0
@bot.event
async def on_guild_join(server):
    print('Joined a new server - ' + server.name)
    new_node = server.id   
    with open('settings.json','r') as f:
       sett = json.load(f)
    sett[new_node] = {}
    sett[new_node]['WELCOME_STATUS'] = False
    sett[new_node]['AUTO_ROLE'] = False
    sett[new_node]['WELCOME_CHANNEL'] = None
    with open('settings.json','w') as f:
       json.dump(sett, f, indent=2)
       print("Total Servers = ",len(sett.keys()))
    game = discord.Game('Vainglory in {} servers'.format(len(sett.keys())))
    await bot.change_presence(status = discord.Status.idle, activity = game)


## DONE v2.0.1
@bot.event
async def on_member_join(member: discord.Member):
    SERVER = str(member.guild.id)
    with open('settings.json','r') as f:
            sett = json.load(f)
    if sett[SERVER]['AUTO_ROLE'] == True:   
        for role in sett[SERVER]['DEFAULT_ROLES']:
            await member.add_roles(discord.Object(id = role))
    if sett[SERVER]['WELCOME_STATUS'] == True:    
        channel = bot.get_channel(sett[SERVER]['WELCOME_CHANNEL'])
        await channel.send(sett[SERVER]['WELCOME_MESSAGE'].format(member.guild.name, member.mention))


## DONE v2.0
@bot.command(pass_context = True)
async def clear(cxt, count: int = 1):
    channel = cxt.message.channel
    try:
        deleted = await channel.purge(limit=count+1)
        await channel.send('Deleted {} message(s)'.format(len(deleted)-1))
        await asyncio.sleep(2)
        await channel.purge(limit=1, check=is_me)
    except discord.errors.Forbidden:
        await cxt.say("Err! Missing permission to delete the messages.")

## DONE v2.0.1
@bot.event
async def on_guild_remove(guild):
    with open('settings.json','r') as f:
       sett = json.load(f)
    sett.pop(str(guild.id), None)
    with open('settings.json','w') as f:
       json.dump(sett, f, indent=2)
    print('Left guild - {}'.format(guild.name))
    print("Total Servers = ",len(sett.keys()))
    print('------')
    game = discord.Game('Vainglory in {} servers'.format(len(sett.keys())))
    await bot.change_presence(status = discord.Status.idle, activity = game)


@bot.command(pass_context = True)
async def invite_bot(cxt):
    await cxt.message.author.send('https://discordapp.com/oauth2/authorize?client_id=448205661935763476&scope=bot')
    await cxt.send('Hey {}! Please check your DM.'.format(cxt.message.author.name))





###################################


@bot.command(pass_context = True)
async def load(cxt, extension_name : str):
    """Loads an extension."""
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await cxt.send("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await cxt.send("{} loaded.".format(extension_name))


@bot.command(pass_context = True)
async def unload(cxt, extension_name : str):
    """Unloads an extension."""
    bot.unload_extension(extension_name)
    await cxt.send("{} unloaded.".format(extension_name))


if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
            print("Successfully loaded extension - " + extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))
    bot.run(config.token)
