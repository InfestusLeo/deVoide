import discord
from discord.ext import commands
import CONFIG
import json, os.path

flag = 0

class Coaching():
    def __init__(self,bot):
        self.bot = bot
    
    @commands.command(pass_context = True)
    async def coach(self, cxt, IGN: str):
        global flag
        ## allowed_roles = (CONFIG.ROLES)
        key_val = str(cxt.message.author.name)
        role = discord.utils.get(cxt.message.author.server.roles, name = 'Back Benchers')    
        
        while flag == 1:
            continue
        flag = 1
        with open('members.json','r') as f:
            coaching = json.load(f)
        coaching.update({ key_val : { 'IGN' : IGN, 'COACHING' : True } })
        with open('members.json','w') as f:
            json.dump(coaching, f, indent=2)
        flag = 0

        await self.bot.add_roles(cxt.message.author, role)
        await self.bot.say('GG {}! Your IGN has been enrolled for coaching'.format(cxt.message.author.name))


    @commands.command(pass_context = True)
    async def view_students(self, cxt):
        coaching, stud_list = {}, ""
        try:
            with open('members.json','r') as f:
                coaching = json.load(f)
        except Exception:
            await self.bot.say('Err, there was an error, locating the file. Please try again!')
        for i in coaching:
            if coaching[i]['Coaching'] == True:
                stud_list += coaching[i]['IGN'] + "\n"
        await self.bot.say(stud_list)
                
            

def setup(bot):
    bot.add_cog(Coaching(bot))
