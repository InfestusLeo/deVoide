import discord
from discord.ext import commands
import CONFIG, json

sett = {}

class Setup():
    def __init__(self,bot):
        self.bot = bot

    ## DONE v2.0.1
    @commands.command(pass_context = True)
    @commands.has_permissions(administrator = True)
    async def welcome_config(self, cxt, channel: discord.TextChannel, *, message: str = "Hi {user}! Welcome to {server} :grin:"):
        """
            Setup a welcome message to welcome new members. This message will be sent everytime a member joins your Guild and the message is sent in the channel you specify.
        """

        global sett
        SERVER = str(cxt.message.guild.id)
        message = message.replace('{user}','{1}').replace('{server}','{0}')

        try:
            with open('settings.json','r') as f:
                sett = json.load(f)

            sett[SERVER]['WELCOME_MESSAGE'] = message
            sett[SERVER]['WELCOME_STATUS'] = True
            sett[SERVER]['WELCOME_CHANNEL'] = str(channel.id)

            with open('settings.json','w') as f:
                json.dump(sett, f, indent=2)
                await cxt.send("```Welcome message set to\t- " + message.replace('{1}','{user}').replace('{0}','{server}') + "\nWelcome channel set to\t- " + channel.name + "```")
        except Exception as f:
            await cxt.sent('Please specify correct arguments--\n?welcome_message <channel> <msg>')
            print(f)
    

    ## DONE 2.0
    @commands.command(pass_context = True)
    async def welcome_channel(self, cxt, channel: discord.TextChannel):
        """
            This command helps you to specify the channel you want the welcome message to be sent everytime a member joins your guild.
        """
        global sett
        SERVER = str(cxt.message.guild.id)

        try:
            with open('settings.json','r') as f:
                sett = json.load(f)
            sett[SERVER]['WELCOME_CHANNEL'] = channel.id
            with open('settings.json','w') as f:
                json.dump(sett, f, indent=2)
                await cxt.send("Updated your welcome channel to -\n" + channel.name)
        except Exception as e:
            print(e)


    @commands.command(pass_context = True)
    async def welcome_status(self, cxt):
        """
            Gives you information about the current welcome status, i.e. your current welcome message, welcome channel and default-roles.
        """
        global sett
        SERVER = str(cxt.message.guild.id)
        with open('settings.json', 'r') as f:
            sett = json.load(f)
        channel = self.bot.get_channel(int(sett[SERVER]['WELCOME_CHANNEL']))
        msg = sett[SERVER]['WELCOME_MESSAGE']
        await cxt.send("```Welcome message set to\t- " + msg.replace('{1}','{user}').replace('{0}','{server}') + "\nWelcome channel set to\t- " + channel.name + "```")


    ## DONE v2.0.1
    @commands.command(pass_context = True)
    async def default_roles(self, cxt, *role: discord.Role):
        """
            Specify the role(s) you want to assign to a member, automatically, when they join your guild.
        """
        global sett
        SERVER = str(cxt.message.guild.id)
        with open('settings.json','r') as f:
            sett = json.load(f)
        sett[SERVER]['DEFAULT_ROLES'] = []

        try:
            for i in role:
                sett[SERVER]['DEFAULT_ROLES'].append(i.id)

            with open('settings.json','w') as f:
                json.dump(sett, f, indent=2)
                await cxt.send("```Updated your default roles to " + ", ".join(i.name for i in role) + "```")
        except Exception as e:
            print(e)


    ## DONE v2.0
    @commands.command(pass_context = True)
    async def auto_role(self, cxt, *, mode: str = None):
        """
            Turn on/off the auto-role module. This, when enabled, automatically assigns your default roles to the new members joining your guild.
        """
        global sett
        SERVER = str(cxt.message.guild.id)

        if mode != None and mode.lower() == 'on':
            new_mode = True
        elif mode != None and mode.lower() == 'off':
            new_mode = False

        with open('settings.json','r') as f:
            sett = json.load(f)
        curr_mode = sett[SERVER]['AUTO_ROLE']

        if mode == None:
            sett[SERVER]['AUTO_ROLE'] = not curr_mode
        else:
            sett[SERVER]['AUTO_ROLE'] = new_mode

        new_mode = sett[SERVER]['AUTO_ROLE']
        with open('settings.json','w') as f:
            json.dump(sett, f, indent=2)
            if new_mode == True:
                await cxt.send("```Enabled auto-role in your guild.```")
            elif new_mode == False:
                await cxt.send("```Disbled auto-role in your guild.```")

    
    ## DONE v2.0
    @commands.command(pass_context = True)
    async def welcome_mode(self, cxt, mode: str = None):
        """
            Turn on/off the welcome module. This, when enabled, sends a welcome message to a specified channel.
        """
        global sett
        SERVER = str(cxt.message.guild.id)

        if mode != None and mode.lower() == 'on':
            new_mode = True
        elif mode != None and mode.lower() == 'off':
            new_mode = False

        with open('settings.json','r') as f:
            sett = json.load(f)
        curr_mode = sett[SERVER]['WELCOME_STATUS']

        if mode == None:
            sett[SERVER]['WELCOME_STATUS'] = not curr_mode
        else:
            sett[SERVER]['WELCOME_STATUS'] = new_mode
        
        new_mode = sett[SERVER]['WELCOME_STATUS']
        with open('settings.json','w') as f:
            json.dump(sett, f, indent=2)
            if new_mode == True:
                await cxt.send("```Enabled welcome messages in your guild.```")
            elif new_mode == False:
                await cxt.send("```Disbled welcome messages in your guild.```")


def setup(bot):
    bot.add_cog(Setup(bot))