import discord
from discord.ext import commands
import CONFIG

class Suggestions():
    def __init__(self,bot):
        self.bot = bot

    @commands.command(pass_context = True)
    async def suggest(self, cxt, *, suggestion: str):
        author = cxt.message.author.name
        channel = self.bot.get_channel(CONFIG.SUGGESTIONS_CHANNEL)
        await channel.send(suggestion + "\n- " + author)


def setup(bot):
    bot.add_cog(Suggestions(bot))
