import discord
from discord.utils import find, get
from discord.ext import commands, tasks
import pyrebase
import re



client = commands.Bot(command_prefix = ".")

config = {
"apiKey": "AIzaSyDrsNEkloCMbB57UH4inku-4hC5qcmTEDc",
"authDomain": "discord-bot-9383a.firebaseapp.com",
"databaseURL": "https://discord-bot-9383a.firebaseio.com",
"projectId": "discord-bot-9383a",
"storageBucket": "discord-bot-9383a.appspot.com",
"messagingSenderId": "972210668590",
"appId": "1:972210668590:web:d226c5f43de72731ba4bbf",
"measurementId": "G-2TW73585EJ"
}
firebase = pyrebase.initialize_app(config)
db = firebase.database()



@client.event
async def on_ready():
    try:
        auth = firebase.auth()
        game = discord.Game(name="with dsicrod API")
        await client.change_presence(activity=game)
    except Exception as e:
        print(e)



@client.event
async def on_member_join(member):
    current_guild = db.child('guilds').child(member.guild.id).get().val()
    if current_guild['welcome-channel-id'] == '' or current_guild['welcome-channel-id'] == None:
        await current_guild.text_channels[0].send(current_guild['welcome-message'].replace('{user}', 'member.mention'))
    else:
        channel = client.get_channel(current_guild['welcome-channel-id'])
        await channel.send(current_guild['welcome-message'].replace('{user}', member.mention).replace('{server}', member.guild.name))
    
    if current_guild['welcome-message-direct'] != '':
        await member.send(current_guild['welcome-message-direct'].replace("{user}",member.name).replace("{server}", member.guild.name))




@client.event
async def on_guild_join(guild):
    server_template = {
        'server-name': '',
        'server-id': '',
        'server-owner-id': '',
        'server-owner-name': '',
        'welcome-channel-id': '',
        'welcome-channel-name': '',
        'welcome-message-direct': '',
        'welcome-message': 'Hello {user}',
        'spam-channel': '',
        'auto-forward-channels': [],
        'auto-role': 'off'
    }


    welcome_channel = find(lambda x: x.name == 'welcome',  guild.text_channels)
    welcome_message = f'Hello, {guild.name}!'
    try:
        if welcome_channel:
            await welcome_channel.send(welcome_message)
        else:
            welcome_channel = guild.text_channels[0]
            await welcome_channel.send(welcome_message)
    except Exception as e:
        print(e)


    server_template['server-name'] = guild.name
    server_template['server-id'] = guild.id
    server_template['server-owner-id'] = guild.owner.id
    server_template['server-owner-name'] = f'{guild.owner.name}#{guild.owner.discriminator}'
    server_template['welcome-channel-id'] = welcome_channel.id
    server_template['welcome-channel-name'] = welcome_channel.name
    print(f'{server_template}\n')


    db.child('guilds').child(server_template['server-id']).set(server_template)




@client.command()
async def set_welcome(ctx, channel: discord.channel, *message: str):
    try:
        db.child('guilds').child(ctx.guild.id).child('welcome-channel-id').set(int(channel[2:-1]))
        db.child('guilds').child(ctx.guild.id).child('welcome-message').set(' '.join(message))
        await ctx.send('Welcome message and channel has been updated successfully.')
    except Exception as e:
        print(e)



@client.command()
async def set_welcome_direct(ctx, *message:str):
    try:
        db.child('guilds').child(ctx.guild.id).child('welcome-message-direct').set(' '.join(message))
        await ctx.send("Direct welcome message changed successfully")
    except Exception as e:
        print(e)
        


@client.command()
async def feedback(ctx, *message):
    channel_id = 636988510313644042
    channel = client.get_channel(channel_id)
    await channel.send(f"""```\n{' '.join(message)}\n{ctx.message.author.name}#{ctx.message.author.discriminator}```""")
    await ctx.send('Your feedback has been submitted successfully.')



@client.command()
async def create_pack(ctx, game_name):
    guild = ctx.message.guild
    category = await guild.create_category(f'{game_name.upper()}')
    await ctx.send('Category created')
    role = await guild.create_role(name=game_name.upper())
    await ctx.message.author.add_roles(role)
    await ctx.send('Role created')
    await guild.create_text_channel(f'{game_name}-lobby', category=category)
    await ctx.send('Lobby created')
    await guild.create_text_channel(f'{game_name}-lounge', category=category)
    await ctx.send('Lounge created')


@client.command()
async def clear(ctx, amount=5, user=None):
    import time
    try:
        if amount<1:
            await ctx.send('Please enter a number greater than 1')
        else:
            await ctx.channel.purge(limit=amount+1)
            await ctx.send(f"{amount} messages deleted")
            time.sleep(3)
            await ctx.channel.purge(limit=amount+1)
    except discord.Forbidden:
        await ctx.send("The bot needs permission to delete messages to serve it's master")


##THIS NEEDS TO BE WORKED ON
# @client.command()
# async def announce_leaders(ctx):
#     _ = set()
#     db_guilds = db.child('guilds').get().val()
#     for server_data in db_guilds.values():
#         for values in server_data.items():
#             try:
#                 _.add(server_data.get('server-owner-id'))
#             except:
#                 pass
#     for user_id in _:
#         user = client.get_user(user_id)
#         await user.send("hi")


@client.command()
async def ping(ctx):
    await ctx.send("Pong!")


#@tasks.loop(seconds=900)
#async def current_status():
#    _members, _servers = 0, 0
#    for items in client.guilds:
#        _members = _members + items.member_count
#        _servers = _servers + 1
#    channel = client.get_channel(639839195552284672)
#    await channel.send(f"{round(client.latency) * 1000} ms - {_servers} servers - {_members} members.")

@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("Please pass in all required arguments.")


@client.command()
async def add_pub_role(ctx, *roles: discord.Role):
    curr_guild = ctx.guild.id
    try:
        guild_pub_roles = db.child('public_roles').child(curr_guild).get().val()
    except Exception:
        guild_pub_roles = []
    for role in roles:
        if role.id not in guild_pub_roles:
            guild_pub_roles.append(role.id)
    print(guild_pub_roles)
    db.child('public_roles').child(curr_guild).set(guild_pub_roles)
    await ctx.send(f"Updated the list of public role(s)")


@client.command()
async def rem_pub_role(ctx, *roles: discord.Role):
    curr_guild = ctx.guild.id
    try:
        guild_pub_roles = db.child('public_roles').child(curr_guild).get().val()
    except Exception:
        guild_pub_roles = []
    for role in roles:
        if role.id in guild_pub_roles:
            guild_pub_roles.remove(role.id)
    print(guild_pub_roles)
    db.child('public_roles').child(curr_guild).set(guild_pub_roles)
    await ctx.send(f"Updated the list of public role(s)")


@client.command()
async def list_pub_role(ctx, *roles: discord.Role):
    curr_guild = ctx.guild.id
    guild_pub_role_names = []
    try:
        guild_pub_roles = db.child('public_roles').child(curr_guild).get().val()
    except Exception:
        guild_pub_roles = []
    if len(guild_pub_roles)>0:
        for role_id in guild_pub_roles:
            role = find(lambda x: x.id == role_id, ctx.guild.roles)
            guild_pub_role_names.append(role.name)
        await ctx.send(f"The current list of public roles is - {guild_pub_role_names}")
    else:
        await ctx.send("No public roles have been added for your server")


@client.command()
async def get_role(ctx, role: discord.Role):
    curr_guild = ctx.guild.id
    try:
        guild_pub_roles = db.child('public_roles').child(curr_guild).get().val()
    except Exception:
        await ctx.send('No public role(s) have been set up for this server')
    if role.id in guild_pub_roles:
        await ctx.message.author.add_roles(role)
        await ctx.author.send(f"Hey {ctx.message.author.name}, you have been granted the role of {role.name}\nin the guild {ctx.guild.name}")


@client.command()
async def grant_role(ctx, member: discord.Member = None, *roles: discord.Role):
    if member == None:
        person = ctx.message.author
    else:
        person = member
    await person.add_roles(*roles)
    role_names = set()
    for role in roles:
        role_names.add(role.name)
    await member.send(f"Hey {member.name}, you have been granted the role(s) of {', '.join(role_names)}\nin the guild {ctx.guild.name}")
    


@client.command()
async def kick(ctx, member: discord.Member, *, reason = None):
    await member.kick(reason=reason)


@client.event
async def on_member_ban(guild, user):
    await user.send(f"Hi {user.name}, you have been banned from {guild.name}. If you think this is an error, please get in touch with the mods.")


@client.event
async def on_member_unban(guild, user):
    await user.send(f"Hi {user.name}, you have been unbanned from {guild.name}. Welcome back.")


@client.command()
async def ban(ctx, member: discord.Member, delete_message_days:int = 1, *, reason = None):
    await member.ban(reason=reason)
    print(reason)
    await ctx.send(f"Successfully banned user {member.name}#{member.discriminator}")


@client.command()
async def unban(ctx, *, member: str):
    banned_users = await ctx.guild.bans()
    member_name, member_discriminator = member.split("#")
    print(member_name, member_discriminator)
    for banned_entry in banned_users:
        user = banned_entry.user
        print((user.name == member_name) and (user.discriminator == member_discriminator))
        if (user.name == member_name) and (user.discriminator == member_discriminator):
            await ctx.guild.unban(user)
            await ctx.send(f"Successfully unbanned user {user.name}#{user.discriminator}")


@client.command()
async def autorole(ctx, status="off", role: discord.Role = None):
    curr_guild = ctx.guild.id
    if status.lower() == "on" and role != None:
        db.child('autoroles').child(curr_guild).set(role.id)
    elif status.lower() == 'on' and role == None:
        ctx.send('Please select a role for the auto-role process')
    else:
        ctx.send('Auto-role disabled')







client.run('Mzg3MTM2OTYwMTM5MTAwMTYw.XbviJA.x1bw8tx-iCmTpcls0eE2FFWIbWU')