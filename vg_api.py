import aiohttp, asyncio, CONFIG, discord, json
import pyvainglory.asyncclient as Client
from discord.ext import commands


class VG_API():
    def __init__(self,bot):
        self.bot = bot

    @commands.command(pass_context = True)
    async def vg_api_status(self, cxt):
        session = aiohttp.ClientSession()
        vgc = Client.AsyncClient(CONFIG.VG_KEY, session)
        await cxt.send(await vgc.get_status())

    @commands.command(pass_context = True)
    async def vg(self, cxt, IGN: str, region: str):
        session = aiohttp.ClientSession()
        vgc = Client.AsyncClient(CONFIG.VG_KEY, session)
        player = await vgc.player_by_name(IGN, region)
        with open('skilltier.json', 'r') as f:
            data = json.load(f)
        session.close()
        await cxt.send(data[player.skill_tier+1]['friendlyName'])
        print(player)
        print(player.skill_tier)


def setup(bot):
    bot.add_cog(VG_API(bot))
