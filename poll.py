import discord
from discord.ext import commands
from datetime import datetime

poll, poll_msg, op, options = False, "", {}, []
q, total, voted = "", 0, set()

class Poll():
    def __init__(self,bot):
        self.bot = bot    

    @commands.command(pass_context = True)
    async def poll(self, cxt, channels: discord.TextChannel, *, string: str):
        global poll_msg, poll, op, options, q, total
        if poll == True:
            await cxt.send('A poll is already running at the moment. Please stop the previous poll to start a new one')
        else:
            query = string.split("<")[0].strip().strip("??poll")
            options = string.split("<")[1].strip(">").split()
            q = query
            total, count = 0, 0
            for i in options:
                op[i] = 0
            msg = "**" + query + "**\n"
            for i in options:
                msg += "{} - {} votes\t\t".format(i, op[i])
                count += 1
                if count%3 == 0:
                    msg += "\n"
            msg += "\n**To vote, please go to your bot-spam channel and type -poll_vote <your choice>\neg: -poll_vote yes*"
            poll_msg = await channels.send(msg)
            poll = True    
    

    @commands.command(pass_context = True)
    async def poll_vote(self, cxt, choice:str = ""):
        if poll == True:
            global poll_msg, op, options, total, voted
            if cxt.message.author.id in voted:
                await cxt.send('Errm, your vote has already been recorded. YOU CHEAT!')
            else:
                try:
                    op[choice] += 1
                    total += 1
                    voted.add(cxt.message.author.id)
                    msg = "**" + q + "**\n"
                    for i in options:
                        msg += "{}-{} votes ({}%)\t\t".format(i, op[i], round((op[i]/total)*100))
                    msg += "\n\n**To vote, please go to your bot-spam channel and type -poll_vote <your choice>\neg: -poll_vote yes*"
                    await poll_msg.edit(content = msg)
                except Exception:
                    await cxt.send('Please provide a valid argument')


    @commands.command(pass_context = True)
    async def poll_stop(self,cxt):
        global poll_msg, poll, total, q, op, voted
        poll_msg, poll, total, op = "", False, 0, {}
        voted.clear()
        channel = self.bot.get_channel(455334489464504330)
        await channel.send('The current poll has been stopped.\n**TOPIC: {}**'.format(q))
        q = ""



def setup(bot):
    bot.add_cog(Poll(bot))
